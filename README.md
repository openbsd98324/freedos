# freedos



## Classic Use for Qemu / freedos


cat fdodin07.144 > /dev/rsd0

zcat freedos-image-hdd.img > /dev/rwd0 


## Install FreeDOS Virtualbox

http://wiki.freedos.org/install/




## BOOT FREEDOS WITH GRUB2 
Add the following in your config scripts for grub2:


```   
 
 menuentry "FreeDOS from NetBSD partition 1, image with raw parameter" {
   set root=(hd0,msdos1)
   linux16 /root/freedos/memdisk/memdisk raw
   initrd16 /root/freedos/fdos1440.img 
 }
 ```   


Tested on GRUB 2.04 with the installatino on netbsd 7.1.1., on a Toshiba 220CS Satellite. 
The first partition is netbsd 7.1.1. (aka. wd0a). 


## Running Win3.1.

```
  qemu-img create -f raw    win31.img   150M  
          qemu-system-i386   -hda win31.img  -fda win31-installation/Disk01.img -boot a  -cpu pentium -m 16 -vga vmware -net nic,model=pcnet -net user -soundhw sb16  ; echo Press Ctrl-Alt-2  and enter  ; echo change floppy0 Disk2.img ; eject floppy0  
        qemu-system-i386   -hda win31.img   -fda msdos/Disk1.img  -boot a  
  qemu-system-i386   -hda win31.img   -fda msdos/Disk1.img  -boot a 

```


## References 
1.) https://wiki.syslinux.org/wiki/index.php?title=MEMDISK#GRUB2

